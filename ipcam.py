from __future__ import print_function
import numpy as np
import cv2
import utils
import os
import sys
import linecache
import glob
import time
import mail
import matplotlib.pyplot as plt
import atexit
import batch_check
import datetime
from multiprocessing import Process
# from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import KMeans
from multiprocessing import Process


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

    def timeStamped():
        fmt='%Y-%m-%d-%H-%M-%S'
        return datetime.datetime.now().strftime(fmt)

    with open("error_log.txt", "a") as text_file:
        text_file.write("\n" + timeStamped())
        text_file.write("\n")
        text_file.write(str(sys.exc_traceback.tb_lineno))
        text_file.write("\n")
        text_file.write('EXCEPTION IN ({}, LINE {} "{}"): {}\n'.format(filename, lineno, line.strip(), exc_obj))


def non_max_suppression_fast(boxes, overlapThresh):
   # if there are no boxes, return an empty list
   if len(boxes) == 0:
      return []
 
   # if the bounding boxes integers, convert them to floats --
   # this is important since we'll be doing a bunch of divisions
   if boxes.dtype.kind == "i":
      boxes = boxes.astype("float")
#  
   # initialize the list of picked indexes   
   pick = []
 
   # grab the coordinates of the bounding boxes
   x1 = boxes[:,0]
   y1 = boxes[:,1]
   x2 = boxes[:,2]
   y2 = boxes[:,3]
 
   # compute the area of the bounding boxes and sort the bounding
   # boxes by the bottom-right y-coordinate of the bounding box
   area = (x2 - x1 + 1) * (y2 - y1 + 1)
   idxs = np.argsort(y2)
 
   # keep looping while some indexes still remain in the indexes
   # list
   while len(idxs) > 0:
      # grab the last index in the indexes list and add the
      # index value to the list of picked indexes
      last = len(idxs) - 1
      i = idxs[last]
      pick.append(i)
 
      # find the largest (x, y) coordinates for the start of
      # the bounding box and the smallest (x, y) coordinates
      # for the end of the bounding box
      xx1 = np.maximum(x1[i], x1[idxs[:last]])
      yy1 = np.maximum(y1[i], y1[idxs[:last]])
      xx2 = np.minimum(x2[i], x2[idxs[:last]])
      yy2 = np.minimum(y2[i], y2[idxs[:last]])
 
      # compute the width and height of the bounding box
      w = np.maximum(0, xx2 - xx1 + 1)
      h = np.maximum(0, yy2 - yy1 + 1)
 
      # compute the ratio of overlap
      overlap = (w * h) / area[idxs[:last]]
 
      # delete all indexes from the index list that have
      idxs = np.delete(idxs, np.concatenate(([last],
         np.where(overlap > overlapThresh)[0])))
 
   # return only the bounding boxes that were picked using the
   # integer data type
   return boxes[pick].astype("int")

def brown_color_checking(color, image):
   if (23 < color[0] and color[0] < 51 
      and 5 < color[1] and color[1] < 19
      and 3 < color[2] and color[2] < 15):
      print ("detected!!!!!!!!!!!!!!!!!!")
      write = Process(target = write_suspened_image, args = (image,2,))
      write.start()
      write.join()
      return 1
   else:
      return 0

def second_brown_color_checking(color, image):
   if (23 < color[0] and color[0] < 51 
      and 5 < color[1] and color[1] < 19
      and 3 < color[2] and color[2] < 15):
      print ("second_detected!!!!!!!!!!!!!!!!!!")
      write =  Process(target = write_suspened_image, args = (image,2,))
      write.start()
      write.join()
      return 1

def police_checking(people, shirt, pants, blank, people_image, original, case):
   clt = KMeans(n_clusters = 3)
   clt.fit(shirt)
   hist = utils.centroid_histogram(clt)
   max_index = hist.argmax()
   max_color = clt.cluster_centers_[max_index]
   
   # second_max_ratio = max(n for n in hist if n!=max(hist))
   # second_max_index = 0;
   # if(second_max_ratio > 0.4):
   #    # Finding the second dominant color
   #    for i in range(len(hist)):
   #       if hist[i]==second_max_ratio:
   #          second_max_index = i
   # second_max_color = clt.cluster_centers_[second_max_index]
   portion = 0
   for i in range(3):
         color = clt.cluster_centers_[i]
         if police_shirt_checking(color)==1:
            portion = portion + hist[i]
#   if (hist[max_index] > 0.4 and police_shirt_checking(max_color)) :
   if portion > 0.4:
         clt.fit(pants)
         hist = utils.centroid_histogram(clt)
         max_index = hist.argmax()
         max_color = clt.cluster_centers_[max_index]
         if police_pants_checking(max_color) == 1:
            name = "./frame/filter_original/" + str(time.time()) + '.jpg'
            cv2.imwrite(name, people)
            if batch_check.main(people, blank) == 1:
               write_suspened_image(people_image, original, 1)
            else:
               return 0

def police_shirt_checking(color):
   if (0 < color[0] and color[0] < 180 
      and 150 < color[1] and color[1] < 253
      and 220 < color[2] and color[2] < 255):
      print ("shirt_detected!!!!!!!!!!!!!!!!!!")

      return 1
   else:
      return 0

def police_pants_checking(color):
   if (35 < color[0] and color[0] < 80
      and 40 < color[1] and color[1] < 80
      and 40 < color[2] and color[2] < 120):
      print ("pants_detected!!!!!!!!!!!!!!!!!!")
      return 1
   else:
      return 0

def write_suspened_image(img, original_img, case):
   img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
   suspened_name = "/home/victor/frame/suspened/" + (str)(img[0][0]) + ".jpg"
   cv2.imwrite(suspened_name, img)
   original_name = "/home/victor/frame/original/" + (str)(img[0][0]) + ".jpg"
   cv2.imwrite(original_name, original_img)
   if case == 1:
      mail.mail_with_attach(["victor.wong@hktaxiapp.com", "project.eye@hktaxiapp.com"],
      "Police Detected",
      "This is a email sent with python",
      suspened_name)
   #   mail.mail_with_attach(["victor.wong@hktaxiapp.com"],
   #   "Police Detected",
   #   "Pants",
   #   suspened_name)
   else:
      mail.mail_with_attach(["victor.wong@hktaxiapp.com", "project.eye@hktaxiapp.com"],
      "Brown Detected",
      "This is a email sent with python",
      suspened_name)      
""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""

name = ""
def main():
   global name
   # initialize the HOG descriptor/person detector
   hog = cv2.HOGDescriptor()
   hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
   # KMeans Model
   clt = KMeans(n_clusters = 3)


   while True:
      bg = cv2.imread("/home/victor/frame/bg/bg.jpg")
      name = max(glob.iglob(os.path.join('/home/victor/frame/','*.png')), key=os.path.getmtime).replace("\\","/")   
      if (name.endswith(".png") and os.path.getsize(os.path.join('/home/victor/frame/',name))):
         try:
            print ("name = ",name)
            frame = cv2.imread(name)
            original = cv2.imread(name[:-3]+'jpg')

            # detect people in the image
            (rects, weights) = hog.detectMultiScale(frame, winStride=(4, 4),
               padding=(8, 8), scale=1.05)

            rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
            pick = non_max_suppression_fast(rects, overlapThresh=0.65) # pick = detected people

            people = frame
            blank = frame
            for (xA, yA, xB, yB) in pick:
               if(xA<0 and yA<0):
                  people = frame[ 0:yB, 0:xB,:]
                  blank = bg[ 0:yB, 0:xB,:]
               elif(yA<0):
                  people = frame[ 0:0+yB, xA:xB,:]
                  blank = bg[ 0:0+yB, xA:xB,:]
               elif(xA<0):
                  people = frame[ yA:yB, 0:0+xB,:]
                  blank = bg[ yA:yB, 0:0+xB,:]
               else:
                  people = frame[ yA:yB, xA:xB,:]
                  blank = bg[ yA:yB, xA:xB,:]

               # Convert the image into RGB
               people_suspended = people.copy()
               people = cv2.cvtColor(people, cv2.COLOR_BGR2RGB)
               people = np.array(people)                       # people for detecting POLICE
               people_image = people                           # for saving the image
               people2 = people                                # people2 for detecting BROWN

               # Remove the bottom part of the people
               people = people[:(int)(people.shape[0]*0.8),(int)(people2.shape[1]*0.3):(int)(people2.shape[1]*0.75),:]

               shirt = people[(int)(people.shape[0]*0.3): (int)(people.shape[0]* 0.6)]
               pants = people[(int)(people.shape[0]*0.5): (int)(people.shape[0]*1), (int)(people.shape[1]*0.2): (int)(people.shape[1]* 0.7)]
               
               # Convert into 2D array
               shirt = shirt.reshape((shirt.shape[0] * shirt.shape[1], 3))
               pants = pants.reshape((pants.shape[0] * pants.shape[1], 3))

               check = Process(target = police_checking, args = (people_suspended, shirt, pants, blank, people_image, original, 1,))
               check.start()
               ################################################################################################
               # Remove the upper half of the image
               people2 = people2[(int)(people2.shape[0]/2):,(int)(people2.shape[1]*0.2):(int)(people2.shape[1]*0.8)]
               people2 = people2.reshape((people2.shape[0] * people2.shape[1], 3))

               clt.fit(people2)
               hist = utils.centroid_histogram(clt)
               max_index = hist.argmax()
               max_color = clt.cluster_centers_[max_index]

               if(brown_color_checking(max_color, people_image)!=1):
                  second_max_ratio = max(n for n in hist if n!=max(hist))
                  if(second_max_ratio > 0.4):
                     # Finding the second dominant color
                     second_max_index = 0;
                     for i in range(len(hist)):
                        if hist[i]==second_max_ratio:
                           second_max_index = i
                     second_max_color = clt.cluster_centers_[second_max_index]
                     second_brown_color_checking(second_max_color, people_image)
            
         except (KeyboardInterrupt):
            exit()
         except:
            PrintException()

def checking_running():
   new_name = ""
   while True:
      time.sleep(10)
      name = max(glob.iglob(os.path.join('/home/victor/frame/','*.png')), key=os.path.getmtime).replace("\\","/")
      os.path.join('/home/victor/frame/',name)
      last_name = name
      print ("last name = ", last_name)
      if new_name == last_name:
         return 0
      else:
         new_name = last_name
         return 1



if __name__ == '__main__':
   main()
  
