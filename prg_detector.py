import os
from multiprocessing import Process
import subprocess
import time
from threading import Thread

cmd1 = 'ps aux | grep \'python ipcam.py$\''
cmd2 = 'ps aux | grep \'python background_capture.py$\''
def ipcam_exec():
	os.system('python ipcam.py')

def bg_cap_exec():
	os.system('python background_capture.py')

while True:
	ipcam = None
	bg_cap = None
	pid1 = None
	pid2 = None
	try:
		time.sleep(3)
		ipcam = subprocess.check_output([cmd1], shell = True)
		if ipcam != None:
			pid1 = ipcam.split()[1]
			print "ipcam.py pid = ", pid1
		bg_cap= subprocess.check_output([cmd2], shell = True)
		if bg_cap!= None:
			pid2 = bg_cap.split()[1]
			print "bg_capture pid = ", pid2
	except(KeyboardInterrupt):
		exit()
	except:
		print "Null"
		if pid1 == None:
			t = Process(target = ipcam_exec)
			t.start()
		elif pid2 == None:
			t = Process(target = bg_cap_exec)
			t.start()
