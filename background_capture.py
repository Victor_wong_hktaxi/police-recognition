from __future__ import print_function
import numpy as np
import cv2
import utils
import os
import sys
import linecache
import glob
import time
import matplotlib.pyplot as plt


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


def non_max_suppression_fast(boxes, overlapThresh):
   # if there are no boxes, return an empty list
   if len(boxes) == 0:
      return []
 
   # if the bounding boxes integers, convert them to floats --
   # this is important since we'll be doing a bunch of divisions
   if boxes.dtype.kind == "i":
      boxes = boxes.astype("float")
#  
   # initialize the list of picked indexes   
   pick = []
 
   # grab the coordinates of the bounding boxes
   x1 = boxes[:,0]
   y1 = boxes[:,1]
   x2 = boxes[:,2]
   y2 = boxes[:,3]
 
   # compute the area of the bounding boxes and sort the bounding
   # boxes by the bottom-right y-coordinate of the bounding box
   area = (x2 - x1 + 1) * (y2 - y1 + 1)
   idxs = np.argsort(y2)
 
   # keep looping while some indexes still remain in the indexes
   # list
   while len(idxs) > 0:
      # grab the last index in the indexes list and add the
      # index value to the list of picked indexes
      last = len(idxs) - 1
      i = idxs[last]
      pick.append(i)
 
      # find the largest (x, y) coordinates for the start of
      # the bounding box and the smallest (x, y) coordinates
      # for the end of the bounding box
      xx1 = np.maximum(x1[i], x1[idxs[:last]])
      yy1 = np.maximum(y1[i], y1[idxs[:last]])
      xx2 = np.minimum(x2[i], x2[idxs[:last]])
      yy2 = np.minimum(y2[i], y2[idxs[:last]])
 
      # compute the width and height of the bounding box
      w = np.maximum(0, xx2 - xx1 + 1)
      h = np.maximum(0, yy2 - yy1 + 1)
 
      # compute the ratio of overlap
      overlap = (w * h) / area[idxs[:last]]
 
      # delete all indexes from the index list that have
      idxs = np.delete(idxs, np.concatenate(([last],
         np.where(overlap > overlapThresh)[0])))
 
   # return only the bounding boxes that were picked using the
   # integer data type
   return boxes[pick].astype("int")

""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""

name = ""
def main():
   global name
   # initialize the HOG descriptor/person detector
   hog = cv2.HOGDescriptor()
   hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())


   rootdir = "/home/victor/frame/"
   while True:
      name = max(glob.iglob(os.path.join('/home/victor/frame/','*.png')), key=os.path.getmtime).replace("\\","/")   
      if (name.endswith(".png") and os.path.getsize(os.path.join('/home/victor/frame/',name))):
         try:
            print ("bg name = ",name)
            frame = cv2.imread(name)
            original = cv2.imread(name[:-3]+'jpg')

            # detect people in the image
            (rects, weights) = hog.detectMultiScale(frame, winStride=(4, 4),
               padding=(8, 8), scale=1.05)

            rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
            pick = non_max_suppression_fast(rects, overlapThresh=0.65) # pick = detected people

            if type(pick) == list:
               cv2.imwrite("/home/victor/frame/bg/bg.jpg", frame)
               time.sleep(3)

         except(KeyboardInterrupt):
         	exit()
     	 except:
            PrintException()
            print ("")


if __name__ == '__main__':
	main()

