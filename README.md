# Police Recognition #
------------------------------------------------------------------------------------
## Program flow ##
1 Use HOG descriptor to detect people.

2 Split the human into upper body(shirt-checking) and lower body(pants checking).

3 Check the shirt and pants color.

   - Use KMeans(clusters = 4) to find the dominating color
   - If both parts satisfy the conditions,save it

4 Check the existence of batch

   - Filter out the background by using background subtraction

   - Find the head position for determining the shoulder position

   - Convert the filtered image into black-and-white

   - Check the black pixel ratio on the shoulder

5 Send and save the image if detected

-----------------------------------------------------------------------

## Program to run ##
* ipcam.py
* snapshot2.py
* background_capture.py
* prg_detector.py (Restart the above programs if they terminated)

## Program Details ##
### ipcam.py ###
The main program
### snapshot2.py ###
Capture the frame from the ip-camera
### background_capture.py ###
Get the "clear" frame from time to time for background subtraction
### background_subtraction.py ###
Filter out the background from the suspended people
### batch_check.py ###
Use the filtered image and find out the position of shoulder and check whether there is a batch or not.