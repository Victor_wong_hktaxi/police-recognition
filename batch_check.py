import cv2
import numpy as np
import os
import background_subtraction
import glob
from sklearn.cluster import KMeans

cs = cv2.CascadeClassifier('./haarcascade_frontalface_alt2.xml')

def head_detection(ori):
    """"""""""""""""""""""""""""""""""""
    # Resize the image to (250, 450)
    # Return the left margin of head 
    """"""""""""""""""""""""""""""""""""

    print "head_detection()"
    ori = cv2.resize(ori, (250, 450))
    head = cs.detectMultiScale(ori)

    head_rectangle = np.empty((1,1))
    for (x, y, w, h) in head:
        rectangle = np.array([[x,y], [x, y+h+10], [x+w, y+h+10], [x+w, y]], np.int32)
    # If it cannot detect the head, return the middle
    if not (type(head) == tuple) and head[0][2] >42 and head[0][1] < 70:
        print "head_corrdinate = {}".format(head)
        return head[0][0], head_rectangle
    else:
        return ori.shape[1]*0.45, head_rectangle



def convert_white_black(no_bg_img):
    """"""""""""""""""""""""""""""""""""
    # Return the resized and filtered image 
    """"""""""""""""""""""""""""""""""""

    print "convert_white_black()"
    no_bg_img = cv2.resize(no_bg_img, (250,450))
    h, w, _ = no_bg_img.shape
    no_bg_img = cv2.cvtColor(no_bg_img, cv2.COLOR_BGR2RGB)

    output = no_bg_img.copy()    # black_white_img
    for x in range(no_bg_img.shape[0]):
        for y in range(no_bg_img.shape[1]):
            if not (no_bg_img[x, y][0] < 120 and no_bg_img[x, y][1]<120 and  no_bg_img[x, y][2] <120):
                output[x,y][0] = 255
                output[x,y][1] = 255
                output[x, y][2] = 255
            else:
                output[x,y][0] = 0
                output[x,y][1] = 0
                output[x, y][2] =0
    return output
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # no_bg_img = cv2.cvtColor(no_bg_img, cv2.COLOR_BGR2RGB)

    # output = no_bg_img.copy()    # black_white_img
    # kmeans_mapped_img = no_bg_img.copy()

    # # convert to 1D array
    # kmeans_mapped_img = kmeans_mapped_img.reshape((kmeans_mapped_img.shape[0] * kmeans_mapped_img.shape[1], 3))

    # clt = KMeans(4)
    # clt.fit(kmeans_mapped_img)
        
    # for x in range(len(clt.labels_)):
    #     kmeans_mapped_img[x] = clt.cluster_centers_[clt.labels_[x]]

    # kmeans_mapped_img = kmeans_mapped_img.reshape((h,w, 3))
    
    # # Filter out the unwanted part if the color is not within the range 
    # for x in range(kmeans_mapped_img.shape[0]):
    #     for y in range(kmeans_mapped_img.shape[1]):
    #         if not (kmeans_mapped_img[x, y][0] < 120 and kmeans_mapped_img[x, y][1]<120 and  kmeans_mapped_img[x, y][2] <120):
    #             output[x,y][0] = 255
    #             output[x,y][1] = 255
    #             output[x, y][2] = 255
    #         else:
    #             output[x,y][0] = 0
    #             output[x,y][1] = 0
    #             output[x, y][2] =0
    # return output, kmeans_mapped_img
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
###############################################################################
def within_range(color):
   if (0 < color[0] and color[0] < 180 
      and 150 < color[1] and color[1] < 255
      and 220 < color[2] and color[2] < 255):
      return 1
   else:
      return 0

def loop_pixel(image):
    for x in range(image.shape[0]):
        count = 0
        for y in range(image.shape[1]):
            if within_range(image[x, y]) == 1:
                count = count + 1
                if count == 8 and not x < 70 and not x > image.shape[0]/2:
                    return x
    return image.shape[0]*0.2


def find_shoulder(filtered):
    filtered = cv2.resize(filtered, (250,450))
    h,w,_ = filtered.shape
    image = cv2.cvtColor(filtered, cv2.COLOR_BGR2RGB)
    image2 = image.reshape((image.shape[0] * image.shape[1], 3))

    clt = KMeans(4)
    clt.fit(image2)
    
    for x in range(len(clt.labels_)):
        image2[x] = clt.cluster_centers_[clt.labels_[x]]

    image2 = image.reshape((h,w, 3))
    # image2 = cv2.cvtColor(image2, cv2.COLOR_RGB2BGR)
    

    shoulder_pos = loop_pixel(image2)
    return shoulder_pos
################################################################################
def shoulder(output, head_pos, shoulder_pos, head_rectangle):
    """"""""""""""""""""""""""""""""""""
    # Cut the shoulder part

    # output : black-white image
    # head_pos : head position array
    # Return the ratio
    """"""""""""""""""""""""""""""""""""
    print "shoulder()"
    print "head_position = ", head_pos

    if head_rectangle.shape[0] ==4: 
        cv2.fillConvexPoly(output, head_rectangle, [255,255,255,0])
    # Cut the left shoulder
    upper_left_b = output[max(shoulder_pos-8,0):shoulder_pos + 30, output.shape[1]*0.25:head_pos+20]

    line = [] 
    for x in range(upper_left_b.shape[1]):
        black = False
        for y in range(upper_left_b.shape[0]):
            if np.all(upper_left_b[y,x] == [0, 0, 0]):
                black = True
                break
        if black == True:
            line.append(1)
        else:
            line.append(0)

    print "line = ", line
    print "\nnumber of 1 = ", line.count(1)
    print "ratio = ", float(line.count(1))/len(line)

    # Find the maximum number of continue "1"
    def good_float(x):
        return "%0.5f" % x
    count = 1
    maxi = 0
    for x in range(len(line)-1):
        if line[x]==line[x+1] and line[x] == 1:
            count = count+1
        if count > maxi:
            maxi = count
        else:
            count = 1
    print "max_continue = ",maxi 
    print "continue_ratio = ",float(good_float(maxi))/float(good_float(len(line)))

    if float(line.count(1))/len(line) >= 0.45:
        return 1
    else:
        return 0
###############################################################################
        

def main(suspended, bg):
    filtered = background_subtraction.run(suspended, bg)
    # filtered = max(glob.iglob(os.path.join('/home/victor/frame/filter/','*.jpg')), key=os.path.getmtime).replace("\\","/")
    filtered = cv2.imread(filtered)
    head, head_rectangle = head_detection(suspended)            # find the head position
    # output, kmeans_mapped_img = convert_white_black(filtered)      # remove the unwanted background
    output = convert_white_black(filtered)      # remove the unwanted background
    shoulder_pos = find_shoulder(filtered)      # find the shoulder(start of body) position

    if shoulder(output, head, shoulder_pos, head_rectangle) ==1:
        return 1
    else:
        return 0

