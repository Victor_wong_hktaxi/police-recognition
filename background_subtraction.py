import numpy as np
from matplotlib import pyplot as plt
import numpy as numpy
import cv2
import time

def run(suspended, bg):
    frameDelta = cv2.absdiff(suspended, bg)
    # frameDelta = cv2.cvtColor(frameDelta, cv2.COLOR_RGB2BGR)
    frameDelta= cv2.GaussianBlur(frameDelta, (5,5), 0)


    frameDelta = cv2.cvtColor(frameDelta, cv2.COLOR_BGR2HSV)
    original = cv2.cvtColor(suspended, cv2.COLOR_BGR2HSV)
    for x in range(frameDelta.shape[0]):
        for y in range(frameDelta.shape[1]):
            if frameDelta[x,y][2] < 70:
                frameDelta[x,y][0] = 0
                frameDelta[x,y][1] = 0
                frameDelta[x,y][2] = 250
            else:
                frameDelta[x,y] = original[x,y]

    frameDelta = cv2.cvtColor(frameDelta, cv2.COLOR_HSV2BGR)
    name = "/home/victor/frame/filter/" + str(time.time()) + ".jpg"
    cv2.imwrite(name, frameDelta)
    return name

