import requests
import urllib
import os
import numpy as np
import cv2
import time
import glob
import mail
from multiprocessing import Process
def load_img():
    count = 0
    # triangle = np.array([[0,0], [0, 500], [480, 0]], np.int32)

    triangle = np.array([[200,0], [640, 0], [640, 472]], np.int32)


    color = [255,255,255, 0]
    while True:
        if count == 150:
          count = 0
        name = "/home/victor/frame/"+ (str)(count) + '.jpg'
        f = open(name,'wb')
        f.write(urllib.urlopen('http://admin:admin@hktaxi.ddns.net/tmpfs/auto.jpg').read())
        f.close()
        img = cv2.imread(name)
        cv2.fillConvexPoly(img, triangle, color)
        img = img[(int)(img.shape[0]*0.1):,(int)(img.shape[1]*0.15):, :]
        # img = img[(int)(img.shape[0]*0.2):,:(int)(img.shape[1]*0.8), :]
        name = "/home/victor/frame/"+ (str)(count) + '.png'
        cv2.imwrite(name,img)
        print ("name = ", name)
        count = count + 1
last_image=""
def get_latest_image():
    global last_image
    while True:
        time.sleep(5)
        print "Inside get_latest_image()"
        latest_image = ""
        latest_image = max(glob.iglob(os.path.join('/home/victor/frame/','*.jpg')), key=os.path.getmtime).replace("\\","/")
        print "latest_image = ",latest_image
        if(latest_image != last_image):
            last_image = latest_image
        else:
            # Checking whether the camera is crashed
            if ipcam_crash() == 1:
                mail.mail_without_attach("victor.wong@hktaxiapp.com", 
                     "The ipcam may be crashed!", 
                     "This mail is sent by Python")
                print "web_cam is crashed!" 
                exit()
            return 0

def ipcam_crash():
    if os.path.getsize(max(glob.iglob(os.path.join('/home/victor/frame/','*.jpg')), key=os.path.getsize)) == 0:
        return 1
    else:
        return 0

def main():
    load = Process(target = load_img)
    load.start()
    while True:
        if get_latest_image() == 0:
            load.terminate()
            load = Process(target = load_img)
            load.start()

if __name__ == "__main__":
    main()

